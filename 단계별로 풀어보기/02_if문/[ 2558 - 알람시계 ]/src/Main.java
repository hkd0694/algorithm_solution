import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {

    public  static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int H = Integer.parseInt(st.nextToken());
        int M = Integer.parseInt(st.nextToken());

        int alarmH = 0;
        int alarmM = 0;

        if(M >= 45) alarmM = M - 45;
        else {
            alarmH ++;
            alarmM = (M + 60) - 45;
        }

        if(alarmH == 1) {
            H--;
            if(H < 0) {
                alarmH = 23;
            } else alarmH = H;
        } else alarmH = H;

        System.out.println(alarmH + " " + alarmM);

        br.close();

    }

}