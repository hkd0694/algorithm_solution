import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int num = Integer.parseInt(br.readLine());

        int index = num * 2 -1;

        for(int i=1;i<=index;i++) {
            int j = 1;
            if(i <= index/2 + 1) {
                for(;j<=i-1;j++) bw.write(" ");
                for(int z = 1; z<= index - (i-1)*2 ;z++) bw.write("*");
            } else {
                for(;j<=index-i;j++) bw.write(" ");
                for(int z = 1; z<= index - (index-i)*2; z++) bw.write("*");
            }

            if(i != index) bw.write("\n");
        }

        bw.flush();
        bw.close();
        br.close();

    }

}