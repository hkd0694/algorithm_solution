import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));


        int num = Integer.parseInt(br.readLine());

        for(int i=1;i<=num*2;i++) {

            if(i % 2 == 1) {
                for(int j=1;j<=num;j++) {
                    if(j % 2 == 1) bw.write("*");
                    else bw.write(" ");
                }
            } else {
                for(int j=1;j<=num;j++) {
                    if(j % 2 == 0) bw.write("*");
                    else bw.write(" ");
                }
            }

            if(i != num*2) bw.write("\n");
        }
        bw.flush();
        bw.close();
        br.close();
    }

}