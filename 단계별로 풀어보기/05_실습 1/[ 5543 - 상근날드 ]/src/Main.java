import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int burger = 2001;
        int drink = 2001;

        for(int i=0;i<5;i++) {
            int num = Integer.parseInt(br.readLine());
            if ( i < 3) {
                if(num < burger) burger = num;
            } else {
                if(num < drink) drink = num;
            }
        }

        int result = burger + drink - 50;

        bw.write(result + "");
        bw.flush();
        bw.close();
        br.close();
    }
}
