import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int star = Integer.parseInt(br.readLine());

        for(int i=1;i<=star*2-1;i++) {
            int j = i;
            if(i <= star) {
                for(int k=1;k<=j;k++) bw.write("*");
            } else {
                for(int z=star*2-1; z>=i;z--) bw.write("*");
            }
            if(i != star*2-1) bw.write("\n");
        }

        bw.flush();
        bw.close();
        br.close();
    }
}