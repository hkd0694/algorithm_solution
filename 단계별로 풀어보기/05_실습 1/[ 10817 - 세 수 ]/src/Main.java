import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {

    public  static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int num1 = Integer.parseInt(st.nextToken());
        int num2 = Integer.parseInt(st.nextToken());
        int num3 = Integer.parseInt(st.nextToken());

        int max = Math.max(num1, Math.max(num2,num3));
        int result;

        if(max == num1) result =  Math.max(num2,num3);
        else if (max == num2) result = Math.max(num1,num3);
        else result = Math.max(num1,num2);

        System.out.println(result);

        br.close();

    }
}