import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) throws Exception{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int sum =0;

        for(int i=0;i<5;i++) {
            int grade = Integer.parseInt(br.readLine());

            if(grade < 40 ) {
                sum += 40;
            } else {
                sum += grade;
            }
        }

        int result = sum / 5;

        bw.write(result + "");
        bw.flush();
        bw.close();
        br.close();

    }
}