import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) throws Exception{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        int test = Integer.parseInt(br.readLine());
        for(int i=0;i<test;i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int length = Integer.parseInt(st.nextToken());
            int arr[] = new int[length];
            int sum = 0;
            for(int j=0;j<arr.length;j++) {
                arr[j] = Integer.parseInt(st.nextToken());
                sum += arr[j];
            }
            double average = (double)sum/(double)length;
            int count = 0;
            for(int z=0;z<arr.length;z++) {
                if(average < (double)arr[z]) count++;
            }
            bw.write(String.format("%.3f%%",((double)count/(double)length)*100) + "\n");
        }
        bw.flush();
        bw.close();
        br.close();
    }
}