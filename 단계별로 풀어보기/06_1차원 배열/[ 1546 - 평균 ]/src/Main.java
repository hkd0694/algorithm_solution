import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int test = Integer.parseInt(br.readLine());

        StringTokenizer st = new StringTokenizer(br.readLine());

        int arr[] = new int[test];
        int max = 0;

        for(int i=0;i<test;i++) {
            arr[i] = Integer.parseInt(st.nextToken());
            if(arr[i] > max) max = arr[i];
        }

        double sum = 0;

        for(int i=0;i<arr.length;i++) sum += (double)arr[i]/max*100;

        bw.write(String.format("%.2f",sum/arr.length));
        bw.flush();
        bw.close();
        br.close();

    }
}