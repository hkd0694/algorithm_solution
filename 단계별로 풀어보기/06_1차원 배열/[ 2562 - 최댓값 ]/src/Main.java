import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int max = -1;
        int index = 0;

        for(int i=1;i<10;i++) {
            int num1 = Integer.parseInt(br.readLine());
            if(num1 > max) {
                max = num1;
                index = i;
            }
        }
        bw.write(max + "\n" + index);
        bw.flush();
        bw.close();
        br.close();
    }

}