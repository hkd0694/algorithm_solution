import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int test = Integer.parseInt(br.readLine());
        StringTokenizer st = new StringTokenizer(br.readLine());

        int num1 = Integer.parseInt(st.nextToken());

        int max = num1;

        int min = num1;

        for(int i=1;i<test;i++) {
            num1 = Integer.parseInt(st.nextToken());
            if(num1 > max) max = num1;
            if(num1 < min) min = num1;
        }
        bw.write(min + " " + max);
        bw.flush();
        bw.close();
        br.close();
    }

}