import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int test = Integer.parseInt(br.readLine());

        for(int i=0;i<test;i++) {
            int floor = Integer.parseInt(br.readLine());
            int ho = Integer.parseInt(br.readLine());

            int arr[][] = new int [floor+1][ho+1];

            for(int j=0;j<arr.length;j++) {
                for(int z=0;z<arr[j].length;z++) {
                    if(j == 0) arr[j][z] = z+1;
                    else {
                        if(z == 0) arr[j][z] = 1;
                        else {
                            arr[j][z] = arr[j][z-1] + arr[j-1][z];
                        }
                    }
                }
            }

            bw.write(arr[floor][ho-1] + "\n");
        }

        bw.flush();
        bw.close();
        br.close();

    }

}
