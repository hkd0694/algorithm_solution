import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int test = Integer.parseInt(br.readLine());

        int count = 0;
        int num = 1;
        boolean check = false;
        int bunmo;
        int bunja;
        while(true) {
            int te = num;
            if(te % 2 == 1) {
                bunmo = 1;
                bunja = te;
            } else {
                bunmo = te;
                bunja = 1;
            }

            do{
                count++;
                if(count == test) {
                    check = true;
                    break;
                }
                if(num % 2 == 1 ) {
                    bunmo++;
                    bunja--;
                } else {
                    bunmo--;
                    bunja++;
                }
                te--;
            }while(te != 0);

            if(check) {
                bw.write(bunja + "/" + bunmo);
                break;
            }
            num++;
        }
        bw.flush();
        bw.close();
        br.close();
    }
}