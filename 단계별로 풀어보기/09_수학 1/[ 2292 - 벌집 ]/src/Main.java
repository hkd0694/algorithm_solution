import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        long test = Integer.parseInt(br.readLine());

        long bang = 1;
        long cha = 0;
        long count = 1;

        while(test > bang) {
            cha += 6;
            bang +=cha;
            count++;
        }

        bw.write(count +"");
        bw.flush();
        bw.close();
        br.close();
    }

}
