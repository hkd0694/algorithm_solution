import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) throws Exception{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int test = Integer.parseInt(br.readLine());

        for(int i=0;i<test;i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int f = Integer.parseInt(st.nextToken());
            int r = Integer.parseInt(st.nextToken());
            int h = Integer.parseInt(st.nextToken());

            int count = 0, ho = 1;

            while(count != h) {

                for(int j=1;j<=f;j++) {
                    count++;
                    if(count == h) {
                        int result = j * 100 + ho;
                        bw.write(result + "\n");
                        break;
                    }
                }
                ho++;
            }
        }

        bw.flush();
        bw.close();
        br.close();
    }

}
