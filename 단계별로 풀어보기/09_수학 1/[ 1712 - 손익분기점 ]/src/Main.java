import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;
public class Main {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        StringTokenizer st = new StringTokenizer(br.readLine());

        long fix = Long.parseLong(st.nextToken());

        long change = Long.parseLong(st.nextToken());

        long price = Long.parseLong(st.nextToken());

        if(change >= price) bw.write("-1");
        else {
            long ch = fix + change;
            long result = 0;
            long count = 0;
            while(ch >= result) {
                count ++;
                ch = fix + (change * count);
                result += price;
            }
            bw.write(count + "");
        }

        bw.flush();
        bw.close();
        br.close();
    }
}