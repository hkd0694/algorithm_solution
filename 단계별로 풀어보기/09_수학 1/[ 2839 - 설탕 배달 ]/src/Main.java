import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception{

       BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
       BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

       int test = Integer.parseInt(br.readLine());
       int count = 0;

       while(test > 0) {
            if(test % 5 == 0) {
                count += test / 5;
                break;
            }
            test -=3;
            count++;
       }

       if(test < 0) bw.write("-1");
       else bw.write(count +"");
       bw.flush();
       bw.close();
       br.close();
    }
}
