import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int cardCount = Integer.parseInt(st.nextToken());
        int M = Integer.parseInt(st.nextToken());
        int card[] = new int[cardCount];
        st = new StringTokenizer(br.readLine());

        for(int i=0;i<cardCount;i++) card[i] = Integer.parseInt(st.nextToken());

        int sum;
        int result = 0;

        for(int i=0;i<=cardCount-2;i++) {
            int j = i+1;
            for(;j<=cardCount-1;j++) {
                int k = j+1;
                for(;k<cardCount;k++)  {
                    sum = card[i] + card[j] + card[k];
                    if( sum > M ) continue;
                    if( M - result > M - sum ) result = sum;
                }
            }
        }
        bw.write(result + "");
        bw.flush();
        bw.close();
        br.close();
    }
}
