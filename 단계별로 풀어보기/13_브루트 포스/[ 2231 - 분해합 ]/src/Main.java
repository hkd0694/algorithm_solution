import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        int bun = Integer.parseInt(br.readLine());

        int result = 0;

        for(int i=1;i<=bun;i++) {
            if(disassemblySum(i , bun)) {
                result = i;
                break;
            }
        }

        bw.write(result + "");
        bw.flush();
        bw.close();
        br.close();
    }

    private static boolean disassemblySum(int number, int result) {
        int sum = number;

        while(number != 0) {
            sum+= number%10;
            number /=10;
        }
        if(sum == result) return true;
        else return false;
    }
}
