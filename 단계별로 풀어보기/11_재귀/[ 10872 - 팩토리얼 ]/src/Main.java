import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));


        int N = Integer.parseInt(br.readLine());
        long result;
        result = fact(N);
        bw.write(result + "");

        bw.flush();
        bw.close();
        br.close();
    }

    //재귀 함수를 이용해서 만든 메서드
    private static long fact(int N) {
        if( N == 0 ) return 1;
        else {
           return N * fact(N-1);
        }
    }

    // for문으로 구현한 팩토리얼 메서드
    private static long factorial(int N) {
        long result = 1;
        for(int i=1;i<=N;i++) {
            result = result * i;
        }
        return result;
    }

}
