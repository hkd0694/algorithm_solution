import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int N = Integer.parseInt(br.readLine());
        int result;
        result = fibo(N);
        bw.write(result + "");

        bw.flush();
        bw.close();
        br.close();
    }

    //재귀 함수를 이용 해서 만든 메서드
    private static int fibo(int N) {
        if(N == 0 ) return 0;
        else if ( N== 1 ) return 1;
        else return fibo(N-1) + fibo(N-2);
    }
}
