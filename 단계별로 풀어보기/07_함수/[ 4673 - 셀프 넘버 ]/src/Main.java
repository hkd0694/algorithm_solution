import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        boolean arr[] = new boolean[10001];


        for(int i=1;i<10001;i++) {
            int selfNumber = selfNum(i);
            if(selfNumber >= 10001) continue;
            arr[selfNumber] = true;
        }

        for(int i=1;i<10001;i++) {
            if(!arr[i]) bw.write(i + "\n");
        }

        bw.flush();
        bw.close();
        br.close();

    }

    public static int selfNum(int num) {
        int result = num;

        while(num / 10 != 0) {
            result += num % 10;
            num /= 10;
        }

        result += num % 10;

        return result;
    }
}