import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int test = Integer.parseInt(br.readLine());

        for(int i=1;i<=test;i++) {
            for(int j=test;j>=1;j--) {
                if(j > i) System.out.print(" ");
                else System.out.print("*");
            }
            System.out.println();
        }
        br.close();
    }
}