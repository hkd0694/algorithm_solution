import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int num = Integer.parseInt(br.readLine());

        int first = num;
        int count = 0;

        do {
            int front = first / 10;
            int end = first % 10;
            first = end * 10 + (front+end)%10;
            count++;
        }while (num != first);

        bw.write(count + "");
        bw.flush();
        bw.close();
        br.close();
    }
}