import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public  static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String num1 = br.readLine();
        String num2 = br.readLine();
        System.out.println(Integer.parseInt(String.valueOf(num2.charAt(2))) * Integer.parseInt(num1));
        System.out.println(Integer.parseInt(String.valueOf(num2.charAt(1))) * Integer.parseInt(num1));
        System.out.println(Integer.parseInt(String.valueOf(num2.charAt(0))) * Integer.parseInt(num1));
        System.out.println(Integer.parseInt(num1) * Integer.parseInt(num2));

    }
}