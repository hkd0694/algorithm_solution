import java.io.*;
import java.util.StringTokenizer;

public class Main {

    public  static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        StringTokenizer st = new StringTokenizer(br.readLine());

        double num1 = Integer.parseInt(st.nextToken());
        double num2 = Integer.parseInt(st.nextToken());

        bw.write(Double.toString(num1/num2));
        bw.flush();
        bw.close();
        br.close();

    }
}