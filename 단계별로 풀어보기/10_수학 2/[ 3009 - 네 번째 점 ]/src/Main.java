import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) throws Exception{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int x[] = new int[4];
        int y[] = new int[4];

        for(int i=0;i<3;i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            x[i] = Integer.parseInt(st.nextToken());
            y[i] = Integer.parseInt(st.nextToken());
        }

        String result = "";

        if(x[0] == x[1]) {
            result += x[2] + " ";
        } else if(x[1] == x[2]){
            result += x[0] + " ";
        } else if(x[0] == x[2]) {
            result += x[1] + " ";
        }

        if(y[0] == y[1]) {
            result += y[2];
        } else if(y[1] == y[2]){
            result += y[0];
        } else if(y[0] == y[2]) {
            result += y[1];
        }

        bw.write(result);

        bw.flush();
        bw.close();
        br.close();

    }
}
