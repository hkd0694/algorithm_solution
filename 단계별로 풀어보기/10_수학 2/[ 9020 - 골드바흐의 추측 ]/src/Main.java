import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    private static boolean init[] = new boolean[10001];

    public static void main(String[] args) throws Exception{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int test = Integer.parseInt(br.readLine());
        eratos();

        for(int i=0;i<test;i++) {
            int num = Integer.parseInt(br.readLine());
            if(num % 2 == 0) {
                int first = num / 2;
                for(int j=first; j>=2;j--)  {
                    if(!init[j] && !init[num-j]) {
                        bw.write(j + " "  + (num-j) + "\n");
                        break;
                    }
                }
            }
        }
        bw.flush();
        bw.close();
        br.close();
    }

    // 1 ~ 10000 까지의 수 중 소수가 아닌 수는 true로 초기화 해주는 함수
    private static void eratos() {
        for(int i=2;(i*i)<=10000;i++) {
            if(!init[i]) {
                for(int j = i*2; j<=10000; j+=i) init[j] = true;
            }
        }
    }

}
