import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    private static boolean init[] = new boolean[250001];

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        init[1] = true;
        eratos();
        while(true) {
            int num = Integer.parseInt(br.readLine());
            int count = 0;
            if(num == 0) break;
            for(int i=num+1; i<=num*2; i++) {
                if(!init[i]) {
                    count++;
                }
            }
            bw.write(count + "\n");
        }
        bw.flush();
        bw.close();
        br.close();
    }

    // 1 ~ 1000000 까지의 수 중 소수가 아닌 수는 true로 초기화 해주는 함수
    private static void eratos() {
        for(int i=2;(i*i)<=250000;i++) {
            if(!init[i]) {
                for(int j = i*2; j<=250000; j+=i) init[j] = true;
            }
        }
    }


}
