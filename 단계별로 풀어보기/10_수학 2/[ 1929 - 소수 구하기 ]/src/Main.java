import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main {

    private static boolean init[] = new boolean[1000001];

    public static void main(String[] args) throws Exception{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        StringTokenizer st = new StringTokenizer(br.readLine());

        int M = Integer.parseInt(st.nextToken());
        int N = Integer.parseInt(st.nextToken());

        eratos();
        //1은 소수가 아니므로 처음에 초기화를 해준다.
        init[1] = true;

        for(int z=M;z<=N;z++) {
            if(!init[z]) {
                bw.write(z + "\n");
            }
        }
        bw.flush();
        bw.close();
        br.close();
    }

    // 1 ~ 1000000 까지의 수 중 소수가 아닌 수는 true로 초기화 해주는 함수
    private static void eratos() {
        for(int i=2;(i*i)<=1000000;i++) {
            if(!init[i]) {
                for(int j = i*2; j<=1000000; j+=i) init[j] = true;
            }
        }
    }

}
