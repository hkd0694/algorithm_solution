# 에라토스테네스의 체

수학에서 에라토스테네스의 체는 소수를 보다 쉽게 찾을 수 있는 방법이다.

알고리즘

1. 2부터 소수를 구하고자 하는 구간의 모든 수를 우선 나열한다.
2. 2는 소수이므로 2의 배수들은 소수가 아니다.
3. 해당 조건에 있는 수들 중 2의 배수인 수들은 모두 지운다.
4. 남아있는 수 가운데 3은 소수이다.
5. 3이 소수이기 때문에 3의 배수 들은 모두 소수가 아니다.
6. 해당 조건에 있는 수들 중에 3의 배수들을 모두 지운다.
7. 위의 방법을 계속해서 탐색하면서 끝까지 가면 소수들만 남아있다..!!

* 만약 120까지의 수를 구한다고 가정하면 11의 제곱은 121 이기떄문에 121 > 120 이므로 11보다 작은 수의 배수들만 지원도 충분하다.

~~~java
// 에라토스테네스의 체 코드 공식
public class Eratos {
	public static void main(String[] args) {
		// ArrayList로 구현
		ArrayList<Boolean> primeList;

		// 사용자로부터의 콘솔 입력
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();

		// n <= 1 일 때 종료
		if(n <= 1) return;

		// n+1만큼 할당
		primeList = new ArrayList<Boolean>(n+1);
		// 0번째와 1번째를 소수 아님으로 처리
		primeList.add(false);
		primeList.add(false);
		// 2~ n 까지 소수로 설정
		for(int i=2; i<=n; i++ )
			primeList.add(i, true);

		// 2 부터  ~ i*i <= n
		// 각각의 배수들을 지워간다.
		for(int i=2; (i*i)<=n; i++){
			if(primeList.get(i)){
				for(int j = i*2; j<=n; j+=i) primeList.set(j, false);
				//i*i 미만은 이미 처리되었으므로 j의 시작값은 i*i로 최적화할 수 있다.
			}
		}
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		for(int i=0; i<=n; i++){
			if(primeList.get(i) == true){
				sb.append(i);
				sb.append(",");
			}
		}
		sb.setCharAt(sb.length()-1, '}');

		System.out.println(sb.toString());

	}
}
