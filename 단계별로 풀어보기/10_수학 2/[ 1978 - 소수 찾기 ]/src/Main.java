import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main {


    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int test = Integer.parseInt(br.readLine());
        StringTokenizer st = new StringTokenizer(br.readLine());
        int count = 0;
        boolean ck;

        for (int i = 0; i < test; i++) {
            int num = Integer.parseInt(st.nextToken());
            ck = minorityCheck(num);
            if (ck) count++;
        }

        bw.write(count + "");
        bw.flush();
        bw.close();
        br.close();
    }

    private static boolean minorityCheck(int num) {
        if (num == 1) return false;
        for (int j = num - 1; j >= 2; j--) {
            if (num % j == 0) return false;
        }
        return true;
    }

}