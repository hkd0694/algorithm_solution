import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int start = Integer.parseInt(br.readLine());
        int end = Integer.parseInt(br.readLine());
        boolean ck = true;
        int min = 0;
        int sum = 0;

        for(int i=start; i<=end;i++) {
            boolean minor = minorityCheck(i);
            if(minor) {
                sum += i;
                if(ck) {
                    min = i;
                    ck = false;
                }
            }
        }

        if(sum == 0) {
            bw.write("-1");
        } else {
            bw.write(sum + "\n" + min);
        }

        bw.flush();
        bw.close();
        br.close();

    }


    private static boolean minorityCheck(int num) {
        if (num == 1) return false;
        for (int j = num - 1; j >= 2; j--) {
            if (num % j == 0) return false;
        }
        return true;
    }

}
