import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        while (true) {
            StringTokenizer st = new StringTokenizer(br.readLine());

            boolean check = true;

            int num1 = Integer.parseInt(st.nextToken());
            int num2 = Integer.parseInt(st.nextToken());
            int num3 = Integer.parseInt(st.nextToken());

            if(num1 == 0 && num2 == 0 && num3 == 0) break;

            int max = Math.max(num1, Math.max(num2,num3));

            if(max == num1) {
                if(num1 * num1 == (num2 * num2) + (num3 * num3)) check = false;
            } else if(max == num2) {
                if(num2 * num2 == (num1 * num1) + (num3 * num3)) check = false;
            } else {
                if(num3 * num3 == (num2 * num2) + (num1 * num1)) check = false;
            }

            if(!check) bw.write("right\n");
            else bw.write("wrong\n");
        }

        bw.flush();
        bw.close();
        br.close();
    }
}
