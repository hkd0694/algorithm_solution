import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        String words = br.readLine();

        words = words.trim();

        long length;

        // 문자열의 공백을 없애고 난 후, 만약 아무 값이 없다면 0으로 초기화를 해주어야 한다.
        if(words.equals("")) {
            length = 0;
        } else {
            length = words.split(" ").length;
        }

        bw.write(length + "");
        bw.flush();
        bw.close();
        br.close();
    }
}