import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        String words = br.readLine();

        words = words.toUpperCase();

        int arr[] = new int[26];

        for (int i = 0; i < words.length(); i++) {
            arr[((int) words.charAt(i) - 65)]++;
        }

        int maxIndex = -1;
        int max = -1;
        boolean check = true;

        for (int j = 0; j < arr.length; j++) {
            if(arr[j] > max) {
                max = arr[j];
                maxIndex = j;
                check = false;
            } else if(arr[j] == max) check = true;
        }

        if(check) bw.write("?");
        else {
            char alpa = (char)(maxIndex+65);
            bw.write(alpa);
        }

        bw.flush();
        bw.close();
        br.close();
    }
}