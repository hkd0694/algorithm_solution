import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int length = Integer.parseInt(br.readLine());

        String num = br.readLine();

        int sum = 0;

        if(length == num.length()) {
            for(int i=0;i<length;i++) {
                sum += Integer.parseInt(String.valueOf(num.charAt(i)));
            }
        }

        bw.write(sum + "");
        bw.flush();
        bw.close();
        br.close();

    }
}