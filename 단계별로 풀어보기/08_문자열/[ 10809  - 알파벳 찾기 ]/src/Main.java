import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int arr[] = new int[26];

        Arrays.fill(arr, -1);

        String data = br.readLine();

        for(int i=0;i<data.length();i++) {
            int check = data.charAt(i);
            if(arr[check - 97] == -1) arr[check-97] = i;
        }

        for(int j=0;j<arr.length;j++) {
            bw.write(arr[j] + " ");
        }

        bw.flush();
        bw.close();
        br.close();
    }

}
