import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        String words = br.readLine();

        int index = 0;
        int count = 0;

        while(index != words.length()) {

            int alb = indexOf(words.charAt(index));

            switch (alb) {
                case 1:
                    if(index + 1 != words.length()) if(words.charAt(index+1) == '=' || words.charAt(index+1) == '-') index++;
                    break;
                case 2:
                    if(index + 1 != words.length()) {
                        if(words.charAt(index+1) == '-') {
                            index++;
                        } else if(words.charAt(index+1) == 'z') {
                            if(index + 2 != words.length()) {
                                if(words.charAt(index+2) == '=') index +=2;
                            }
                        }
                    }
                    break;
                case 3:
                case 4:
                    if(index + 1 != words.length()) if(words.charAt(index+1) == 'j') index++;
                    break;
                case 5:
                case 6:
                    if(index + 1 != words.length()) if(words.charAt(index+1) == '=') index++;
                    break;
            }
            count++;
            index++;
        }

        bw.write(count + "");
        bw.flush();
        bw.close();
        br.close();

    }

    private static int indexOf(char alpa) {

        if(alpa == 'c') {
            return 1;
        } else if(alpa == 'd') {
            return 2;
        } else if(alpa == 'l') {
            return 3;
        } else if(alpa == 'n') {
            return 4;
        } else if(alpa == 's') {
            return 5;
        } else if(alpa == 'z') {
            return 6;
        } else {
            return 0;
        }

    }
}
