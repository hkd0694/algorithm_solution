import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        StringTokenizer st = new StringTokenizer(br.readLine());

        String num1 = st.nextToken();
        String num2 = st.nextToken();

        int intNumber1 = reverse(num1);
        int intNumber2 = reverse(num2);

        if(intNumber1 > intNumber2) bw.write(intNumber1 + "");
        else bw.write(intNumber2 + "");

        bw.flush();
        bw.close();
        br.close();
    }


    private static int reverse(String num) {

        String change = "";

        for(int i=num.length()-1;i>=0;i--) {
            change += num.charAt(i);
        }

        int result = Integer.parseInt(change);

        return result;
    }

}
